<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    return [
        'title' => rtrim( $faker->sentence(rand(5 , 10)), "." ), //generates 5 - 10 random words
        'body' => $faker->paragraphs(rand(3 , 7), true), // if TRUE in 2nd arg, each sentence will be separated by newline, else it will return array
        'views' => rand( 0, 10 ),
        // 'answers_count' => rand( 0, 10 ),
        // 'votes_count' => rand( -3, 10 ), // will allow negative values
    ];
});
