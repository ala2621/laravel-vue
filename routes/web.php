<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/* 
	to override the show method to accept slug and not question for more SEO friendly
*/
Route::resource('questions', 'QuestionsController')->except('show'); // will exclude 'show'
Route::get('/questions/{slug}', 'QuestionsController@show')->name('questions.show');

// Route::post('/questions/{questions}/answers', 'AnswerController@store')->name('answers.store');
// Route::resource('questions.answers', 'AnswersController')->(['store', 'edit', 'update', 'destroy']);

Route::resource('questions.answers', 'AnswersController')->except(['create', 'show ']);

Route::post('/answers/{answer}/accept', 'AcceptAnswerController')->name('answers.accept');

Route::post('/questions/{question}/favorites', 'FavoritesController@store')->name('questions.favorite');
Route::delete('/questions/{question}/favorites', 'FavoritesController@destroy')->name('questions.unfavorite');

Route::post('/questions/{question}/vote', 'VoteQuestionController');
Route::post('/answers/{answer}/vote', 'VoteAnswerController');