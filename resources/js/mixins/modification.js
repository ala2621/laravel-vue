export default {
	data() {
		return {
			editing: false,
		}
	},

	methods: {
		edit() {
			this.setEditCache();
			this.editing = true; //show the form
		},

		cancel() {
			this.restoreFromCache();
			this.editing = false; //hide the form
		},

		setEditCache() {},
		restoreFromCache() {},


		update() {
			axios.put(this.endpoint, this.payload())
			.catch( ({response}) => {
				this.$toast.error(response.data.message, "Error", { timeout: 3000});
			})
			.then( ({data}) => {
				this.bodyHtml = data.body_html;
				this.$toast.success(data.message, "Success", { timeout: 3000});
				this.editing = false; //hide the form
			})
		},

		payload() {},

		destroy() {
			this.$toast.question('Are you sure about that?',"Confirm Deletion", {
			    timeout: 20000,
			    close: false,
			    overlay: true,
			    displayMode: 'once',
			    id: 'question',
			    zindex: 999,
			    position: 'center',
			    buttons: [
			        ['<button><b>YES</b></button>', (instance, toast) => {
			 			
						this.delete();						

			            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
			 
			        }, true],
			        ['<button>NO</button>', function (instance, toast) {
			 
			            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
			 
			        }],
			    ],
			});
		},

		delete() {},

	}
}