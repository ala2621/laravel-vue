@extends('layouts.app')

@section('content')
<section id="questions">
    <div class="container">
        
        <question-page :question="{{ $question }}"></question-page>

       {{--  @include ('answers._index', [
            'answers'           => $question->answers,
            'answersCount'      => $question->answers_count,
        ]) --}}


        {{-- @include ('answers._create', []) --}}
    </div>
</section>
@endsection
