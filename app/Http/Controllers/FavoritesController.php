<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Question;

class FavoritesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth'); //ensures the user is signed in
	}

    public function store (Request $request, Question $question)
    {
    	$question->favorites()->attach(auth()->id());
        if ( $request->expectsJson() ) {
            return response()->json(null, 204);
        }

        return back();
    }

    public function destroy (Request $request, Question $question)
    {
    	$question->favorites()->detach(auth()->id()); //destroy the relationship
        if ( $request->expectsJson() ) {
            return response()->json(null, 204);
        }

        return back();
    }
}
