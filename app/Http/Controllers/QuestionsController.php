<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use App\Http\Requests\AskQuestionRequest;

class QuestionsController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => ['index', 'show', ]]); //THIS WILL LIMIT ALL THE PAGES except the slug of the pages included in the parameter
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::with('user')->latest()->paginate(5);
        
        // THE COMMENTED CODES WILL DISPLAY THE DETAILS OF THE QUERY
        // \DB::enableQueryLog(); // debugging purposes
        // view('questions.index', compact('questions'))->render(); // debugging purposes
        // dd(\DB::getQueryLog()); // debugging purposes


        return view('questions.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $question = new Question();

        return view('questions.create', compact('question'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AskQuestionRequest $request)
    {
        $request->user()->questions()->create($request->all()); // $request->only('title', 'body')  //OR IF YOU WANT TITLE ONLY

        return redirect()->route('questions.index')->with('success', "Your question has been submitted"); 
        // or redirect('/questions')->with('success', "Your question has been updated "); TO REDIRECT ONLY
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        $question->increment('views'); //INCREMENT VIEWS FIELD BY 1

        return view('questions.show', compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        // if (\Gate::allows('update-question', $question)) {    
        //     return view("questions.edit", compact('question'));
        // }
        // abort(403, "Access Denied");

        // negated story 
        if (\Gate::denies('update-question', $question)) {       
            abort(403, "Access Denied");
        }
        return view("questions.edit", compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(AskQuestionRequest $request, Question $question)
    {
        if (\Gate::denies('update-question', $question)) {       
            abort(403, "Access Denied");
        }

        // return view("questions.edit", compact('question')); // NOT NEEDED THIS

        $question->update($request->only('title', 'body'));

        if ($request->expectsJson()) {
            return response()->json([
                'message'   => "Your question has been updated.",
                'body_html' => $question->body
            ]);
        }

        return redirect()->route('questions.index')->with('success', "Your question has been updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        if (\Gate::denies('delete-question', $question)) {       
            abort(403, "Access Denied");
        }
        // return view("questions.edit", compact('question'));
        $question->delete();

        if ( request()->expectsJson() ) {

            return response()->json([
                'message'   => "Your question has been deleted."
            ]);

        }

        return redirect()->route('questions.index')->with('success', "Your question has been deleted.");
    }
}
