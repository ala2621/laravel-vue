<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use VotableTrait;
    
	protected $fillable = ['body', 'user_id'];

    protected $appends = ['created_date', 'body_html', 'is_best'];
	
    public function question()
    {
    	return $this->belongsTo(Question::class);
    }

	public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function getBodyHtmlAttribute()
    {
        return clean( \Parsedown::instance()->text($this->body) );
    }

    public static function boot() 
    {
    	parent::boot();
    	// EVENTS 
    	//will occur when a record is CREATED
    	static::created(function($answer){
    		$answer->question->increment('answers_count');
    		// $answer->question->save();     NOT NECCESSARY HERE
    	});

    	//will occur if a record is UPDATED
    	// static::saved(function($answer){
    	// 	echo "Answer saved\n";
    	// });

        //will occur when a record is DELETED
        static::deleted(function($answer){
            $answer->question->decrement('answers_count');
            // if ( $question->best_answer_id === $answer->id ) {
            //     $question->best_answer_id = NULL;
            //     $question->save();
            // }

        });
    }

	public function getCreatedDateAttribute() 
    {
    	return $this->created_at->diffForHumans(); //makes the date in "3 days ago format"
    }

    public function getStatusAttribute()
    {
        return $this->isBest() ? 'vote-accepted' : '';
    }

    public function getIsBestAttribute()
    {
        return $this->isBest();
    }

    public function isBest()
    {
        return $this->id === $this->question->best_answer_id;
    }
}